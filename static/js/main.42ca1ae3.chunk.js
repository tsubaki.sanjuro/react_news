(window["webpackJsonpreactwarriors-test-task-basic"] = window["webpackJsonpreactwarriors-test-task-basic"] || []).push([
	[0], {
		11: function (t, e, n) {
			t.exports = n(17)
		},
		16: function (t, e, n) {},
		17: function (t, e, n) {
			"use strict";
			n.r(e);
			var a = n(1),
				r = n.n(a),
				i = n(8),
				c = n.n(i),
				o = n(2),
				s = n(3),
				m = n(5),
				l = n(4),
				u = n(6),
				d = n(10),
				h = (n(16), function (t) {
					function e() {
						return Object(o.a)(this, e), Object(m.a)(this, Object(l.a)(e).apply(this, arguments))
					}
					return Object(u.a)(e, t), Object(s.a)(e, [{
						key: "render",
						value: function () {
							var t = this.props.data;
							return r.a.createElement("div", {
								key: t.id,
								className: "reddit__thread"
							}, r.a.createElement("img", {
								src: t.thumbnail,
								alt: "img"
							}), r.a.createElement("h3", null, "Title: ", t.title), r.a.createElement("p", null, t.num_comments, " comments"), r.a.createElement("a", {
								href: "https://www.reddit.com/".concat(t.permalink),
								target: "_blank",
								rel: "noopener noreferrer"
							}, "Link"))
						}
					}]), e
				}(r.a.Component)),
				p = function (t) {
					function e(t) {
						var n;
						return Object(o.a)(this, e), (n = Object(m.a)(this, Object(l.a)(e).call(this, t))).state = {
							items: [],
							isLoading: !1
						}, n
					}
					return Object(u.a)(e, t), Object(s.a)(e, [{
						key: "componentDidMount",
						value: function () {
							var t = this;
							this.setState({
								isLoading: !0
							}), fetch("https://www.reddit.com/r/reactjs.json?limit=100").then((function (t) {
								return t.json()
							})).then((function (e) {
								var n = e.data;
								t.setState({
									items: n.children,
									isLoading: !1
								})
							}))
						}
					}, {
						key: "render",
						value: function () {
							var t = this.state,
								e = t.items,
								n = t.isLoading,
								a = e.sort((function (t, e) {
									return e.data.num_comments - t.data.num_comments
								}));
							return r.a.createElement("div", {
								className: "reddit"
							}, r.a.createElement("h1", null, "Top comments"), n ? r.a.createElement(d.a, {
								color: "#123abc",
								loading: this.state.isLoading
							}) : a.map((function (t) {
								return r.a.createElement(h, {
									key: t.data.id,
									data: t.data
								})
							})))
						}
					}]), e
				}(r.a.Component);
			c.a.render(r.a.createElement(p, null), document.getElementById("root"))
		}
	},
	[
		[11, 1, 2]
	]
]);
//# sourceMappingURL=main.42ca1ae3.chunk.js.map